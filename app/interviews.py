# -*- coding: utf-8 -*-

'''
Module that runs allows to run a Bottle server which organizes your data
in order to generate a webtoprint document that takes into account all
the interviewees and their interviews.
'''

from bs4 import BeautifulSoup as bs
from bottle import route, run

from data_utils import get_data_from_ods, get_interviewees_from_ods
from server import (ODS_FILE, get_rows, format_rows, 
    readlines_to_str, get_all_interviewees, sanitize)
from g_pyvis import write_to_file
from titles import get_title, TITLE_MODE

INTERVIEWS_HTML_TEMPLATE = 'html/interviews.html'
INTERVIEWS_TMP_FILE = 'tmp/interviews.html'

@route('/')
def index():
    df = get_data_from_ods(ODS_FILE)
    # Formats a correct DataFrame out of the ODS file
    df_interviewees = get_interviewees_from_ods(ODS_FILE)

    names = get_all_interviewees(df_interviewees, join = False)
    # Get the interviews names' list in the right order

    make_interviews_html(df, names)
    return readlines_to_str(INTERVIEWS_TMP_FILE)

def make_interviews_html(df, names, show_title=True):
    with open(INTERVIEWS_HTML_TEMPLATE) as html:
        soup = bs(html, 'html.parser')
        interviews = soup.find("section", {"id": "interviews"})
        for idx in range(len(names)):
            interview = soup.new_tag("section", attrs={"class": "interview", "id": idx})

            if show_title:
                title = soup.new_tag("h1", attrs={"class": "name"})
                if TITLE_MODE == "names": 
                    title.string = " ".join(names[idx])
                else:
                    title.string = get_title(name=names[idx])
                interview.append(title)

            insert_details_html(df, names[idx], interview)
            interviews.append(interview)
            #insert_page_break(soup, interviews)

    html.close()
    soup = soup.prettify()
    s = sanitize(soup)
    write_to_file(s, INTERVIEWS_TMP_FILE)

def insert_details_html(df, name, interview):
    details = format_rows(get_rows(df, name[0], name[1], False), False)
    for detail in details:
        interview.append(detail)

if __name__ == '__main__':
    run(host='localhost', port=8081, debug=True)
