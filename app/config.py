'''
Module that contains constants you must tweak to comply to your configuration
'''

# absolute path to the executable of your geckodriver, usually in /usr/bin on UNIX systems
PATH_TO_GECKODRIVER = "/usr/bin/geckodriver"
DEFAULT_PRINTER = "default_printer_name_as_mentioned_by_your_lpstat_command"
