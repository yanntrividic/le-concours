'''
Module dedicated to the generation of titles for the citations' PDFs.
A modest cryptographic algorithm was implemented in order to anonymize
the interviewees.
'''

import random
#import unidecode

TITLE_MODE = "dict" # can be "dict", "random", "simple" or "names"
TITLE = "Contributions &amp; dépannages"
TITLES = ["acolytes", "mains-fortes", "coups de pouce", "coopération", "appuis", 
            "complicité", "inspirations", "soutien", "concours", "dépannages", "contributions",
            "coïncidence", "support", "inspirations", "interventions", "collaboration",
            "formes d’aide", "assistance", "secours"]
#TITLE = "Concours de circonstances"
#TITLE = "Coups de pouce"

TITLES_DICT = {
    'A': ["acolytes", "appuis", "assistance", "aides"],
    'B': ["contributions", "collaboration"],
    'C': ["concours", "coopération", "coïncidence", "coups de pouce", "complicité"],
    'D': ["dépannages", "coïncidence", "dons"],
    'E': ["interventions", "compréhension", "secours"],
    'F': ["mains-fortes", "réconfort"],
    'G': ["dépannages", "refuge", "korrigans"],
    'H': ["repêchages", "compréhension"],
    'I': ["inspirations", "coïncidence", "interventions"],
    'J': ["conjonctures", "objectifs", "réajustages"],
    'K': ["eurêkas", "karma", "korrigans"],
    'L': ["collaboration", "complicité"],
    'M': ["mains-fortes", "complicité", "mutualité"],
    'N': ["inspirations", "connivence"],
    'O': ["coïncidence", "collaboration"],
    'P': ["dépannages", "coups de pouce", "appuis", "complicité"],
    'Q': ["questions", "quidams", "Quat’z’Arts"],
    'R': ["contributions", "coopération", "inspirations"],
    'S': ["assistance", "secours", "soutien"],
    'T': ["contributions", "complicité"],
    'U': ["support", "appuis", "mutualité"],
    'V': ["connivence", "interventions"],
    'W': ["sandwichs"],
    'X': ["expériences", "auxiliaires"],
    'Y': ["acolytes", "sympathies"],
    'Z': ["zélotes", "Quat’z’Arts"]
}

def get_title(mode="dict", name=""):
    if mode == "random":
        return get_random_title()
    if mode == "dict":
        return get_title_initials(name)
    if mode == "simple":
        return TITLE

def format_keywords_into_title(keywords):
    formatted_title = ""
    for idx in range(len(keywords)):
        formatted_title = formatted_title + keywords[idx]
        if idx == 0 : formatted_title = formatted_title.capitalize()
        if idx < len(keywords)-2:
            formatted_title = formatted_title + ", "
        elif idx == len(keywords)-2:
            formatted_title = formatted_title + " &amp; "
    return formatted_title

def get_random_title():
    s1 = random.randint(0, len(TITLES)-1)
    s2 = random.randint(0, len(TITLES)-1)
    while s1 == s2:
        s2 = random.randint(0, len(TITLES)-1)

    return format_keywords_into_title([TITLES[s1], TITLES[s2]])

def get_initials(name):
    without_dashes = " ".join(name).replace('-',' ')
    #unaccented_string = unidecode.unidecode(without_dashes) # removes diacritics
    list_of_names = without_dashes.split(' ')
    while '' in list_of_names: #removes the empty strings for the "--" sequence or else
        list_of_names.remove('')

    initials = []
    for name in list_of_names:
        initials.append(name[0].capitalize())
    return initials

def get_title_initials(name):
    assert name != "" # si on a pas rempli d'argument avec le mode "dict"
    initials = get_initials(name)
    titles_keywords = []
    for idx in range(len(initials)):
        keyword = random.choice(TITLES_DICT[initials[idx]])
        while keyword in titles_keywords and len(TITLES_DICT[initials[idx]]) > 1:
            keyword = random.choice(TITLES_DICT[initials[idx]])
            print(keyword)
        titles_keywords.append(keyword)
    return format_keywords_into_title(titles_keywords)

if __name__ == '__main__':
    print(get_title(mode="dict", name=["Hassan", " "]))
