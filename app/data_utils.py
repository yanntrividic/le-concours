# -*- coding: utf-8 -*-

'''
Series of functions that help make the ODS file into a more convenient form to work with.
This module also hosts certain methods to format the data and make it gephi-complient.
Everything that's related to Gephi in there is buggy and not maintained.
'''

import pandas as pd
import unidecode
from pandas_ods_reader import read_ods
from nltk.metrics.distance import edit_distance

UNDIRECTED_NODES_CSV_FILE = 'data/undirected_nodes.csv'
UNDIRECTED_EDGES_CSV_FILE = 'data/undirected_edgess.csv'

typo = [('c', 'conceptuelle'),
        ('th', 'théorique'),
        ('a', 'assistanat'),
        ('m','matérielle'),
        ('te', 'technique'),
        ('e', 'émotionnelle'),
        ('s', 'sociale')]

def read_ods_to_df(path, sheet = 1):
    '''Reads a CSV file to make it fit into a dataframe, the delimiter is set to ',', 
    the file has to have a header row and everything is set to default according 
    to the Dataframe.to_csv documentation
    '''
    try:
        df = read_ods(path, sheet)  
        # i.e. the file HAS TO have a header as first row and ',' seps.
        return df
    except FileNotFoundError:
        print(path, ' could not be read.')
        return None

def read_csv_to_df(path):
    '''Reads a CSV file to make it fit into a dataframe, the delimiter is set to ',', 
    the file has to have a header row and everything is set to default according 
    to the Dataframe.to_csv documentation
    '''
    try:
        df = pd.read_csv(path, delimiter=',', header=0)  
        # i.e. the file HAS TO have a header as first row and ',' seps.
        return df
    except FileNotFoundError:
        print(path, ' could not be read.')
        return None

def format_typo(value):
    '''Reads a String formatted as a list of words separated by a comma, and makes it as a list of unaccentuated strings.
    Useful from processing the typology afterwards.
    TODO: Make it so it corrects typos and only words of typo appear.
    '''
    unaccented_string = unidecode.unidecode(str(value))
    l = unaccented_string.split(', ')
    l = check_and_replace_typo(l)
    if l[0] == 'tout': l = typo[:-1]
    return l

def check_and_replace_typo(l):
    '''Checks if every word is in the typo list, and corrects it if not.
    '''
    typo_ids = list(zip(*typo))[0]
    for i in range(len(l)):
        if l[i] in typo_ids:
            l[i] = typo[typo_ids.index(l[i])][1]
        else:
            print(l)
    return l

def find_closest_word(target, l):
    '''Finds the word in the l list that is closest to target and returns it
    '''
    res = []
    for w in l:
        d = edit_distance(w, target)
        res.append((d, w))

    return target

def find_unique_names(df):
    s = set()
    for idx, row in df.iterrows():
        author, cdp = get_names_from_row(row)
        s.add(author)
        s.add(cdp)
    return s

def get_names_from_row(row):
    return str(row['prenom_auteur']) + " " + str(row['nom_auteur']), 
    str(row['prenom_cite']) + " " + str(row['nom_cite'])

def give_ids_to_names(s):
    d = {}
    i = 0
    for n in s :
        d[n] = i
        i = i + 1 
    return d

def gen_undirected_unweighted_gephi_date(df):
    s = find_unique_names(df)
    ids = give_ids_to_names(s)
    edges = pd.DataFrame(columns=['Id', 'Source', 'Target', 'Type'])

    l = []
    for idx, row in df.iterrows():
        source, target = get_names_from_row(row)
        new_row = {'Source': ids[target], 
                    'Target': ids[source],
                    'Type': "Undirected"}
        if new_row not in l:
            tmp = new_row['Source']
            new_row['Source'] = new_row['Target']
            new_row['Target'] = tmp
            if new_row not in l:
                new_row['Id'] = idx
                l.append(new_row)

    edges = pd.concat([edges, pd.DataFrame.from_records(l)])
    nodes = ids_to_df(ids)

    return nodes, edges

def gen_directed_weighted_gephi_data(df):
    s = find_unique_names(df)
    ids = give_ids_to_names(s)
    edges = pd.DataFrame(columns=['Id', 'Source', 'Target', 'Type', 'Label', 'Weight', 'Typo'])

    l = []
    for idx, row in df.iterrows():

        source, target = get_names_from_row(row)

        if row['donne_recu'] == 'd':
            new_row = {'Source': ids[source], 'Target': ids[target], 'Label': source + " vers " + target}
            new_row['Type'] = 'Directed'
        elif row['donne_recu'] == 'r':
            new_row = {'Source': ids[target], 'Target': ids[source], 'Label': target + " vers " + source}
            new_row['Type'] = 'Directed'
        elif row['donne_recu'] == 'dr':
            new_row = {'Source': ids[target], 'Target': ids[source], 'Label': source + " et " + target}
            new_row['Type'] = 'Undirected'

        new_row['Weight'] = len(row['typo'])
        new_row['Typo'] = row['typo']
        new_row['Id'] = idx
        l.append(new_row)

    edges = pd.concat([edges, pd.DataFrame.from_records(l)])
    nodes = ids_to_df(ids)

    return nodes, edges

def ids_to_df(ids):
    nodes = pd.DataFrame(columns=['Id', 'Label'])
    new_rows = []
    for key in ids.keys():
        new_row = {'Id': ids[key], 'Label': key}
        new_rows.append(new_row)

    nodes = pd.concat([nodes, pd.DataFrame.from_records(new_rows)])
    return nodes

def merge_rows_typos(r1, r2):
    return list(set(r1["typo"]).union(set(r2["typo"])))

def save_data_to_csv(data, nodes_file, edges_file):
    nodes, edges = data
    nodes.to_csv(nodes_file, index=False)
    edges.to_csv(edges_file, index=False)
    print("CSV files saved.")

def clean_gephi_edges(edges):
    # split the edges in directed and undirect dataframe:
    directed = edges[edges['Type'] == 'Directed']
    undirected = edges[edges['Type'] == 'Undirected']

    print("directed\n", directed)
    #First we add the directed edges to the same existing undirected edges
    for idx, row in undirected.iterrows():
        #print("before\n", undirected.loc[[idx]])
        # we get all the directed edges that are in the right order
            
        # and add the directed edges that are in the reversed order
        indexes = indexes + directed[(directed['Target'] == row['Source']) & (directed['Source'] == row['Target'])].index.tolist()

        typos = concat_edges_typos(row, directed, indexes) # concatenation of the typologies
        directed = directed.drop(indexes) # drop of the useless edges

        undirected.at[idx, 'Typo'] = list(typos)
        undirected.at[idx, 'Weight'] = len(typos)
        #print("after\n", undirected.loc[[idx]])
    print("directed\n", directed)
    return pd.concat([undirected, directed], axis=0)

def concat_edges_typos(edge, edges, indexes):
    '''Function that concatenates the typologies of a series of edges and removes the edges from the egdes DataFram
    '''
    # concatenation of the typologies
    typos = edge['Typo']
    for i in indexes:
        for l in edges.loc[[i]]['Typo'].tolist() :
            for t in l: 
                if t not in typos:
                    #print(t)
                    typos.append(t)
    return typos

def fill_empty_details(df):
    empty_details_indexes = df[df['details'].isnull()].index.tolist()
    for i in empty_details_indexes:
        df.at[i, 'details'] = generate_sent_from_typo(df.iloc[i]['typo'])
    return df

def generate_sent_from_typo(typos):
    if len(typos) == 7:
        return "a aidé pour tout plein de choses très différentes"

    tmp = typos.copy()
    s = ""

    if "assistanat" in typos:
        s = s +"a assisté de différentes manières"
        tmp.remove("assistanat")
        if tmp: s = s + " # "

    if tmp:
        s = s + "a apporté une aide "
        for typo in tmp:
            s = s + typo +", "
        s = s[:-2]

    return s

def split_when_several_cdp(df):
    tmp_df = pd.DataFrame(columns=df.columns) # we make a new and empty DataFrame
    tmp_rows = []

    for idx, row in df.iterrows():
        split = row['details'].split(' # ')

        if len(split) > 1: # in the case we must split the row and add new rows
            for detail in split:
                tmp_row = row.copy()
                tmp_row['details'] = detail
                tmp_rows.append(tmp_row)

        else: # in the case we don't have anything to do
            tmp_rows.append(row)

    # finally, we return the new df
    return pd.concat([tmp_df, pd.DataFrame.from_records(tmp_rows)])

def get_data_from_ods(path):
    df = read_ods_to_df(path, 2) # reads the second sheet on the ODS file
    df = df.drop("id", axis=1) # drop the id columns

    df['typo'] = df['typo'].apply(format_typo)

    df = fill_empty_details(df) # uses the typo columns to fill the details column
    # at this point, all the details column is filled.

    df = split_when_several_cdp(df)

    return df

def get_interviewees_from_ods(path):
    df = read_ods_to_df(path, 1) # reads the third sheet on the ODS file
    return df

if __name__ == '__main__':
    df = get_data_from_ods('../suivi_interviews.ods')
    data = gen_undirected_unweighted_gephi_date(df)
    nodes, edges = data

    save_data_to_csv(data, UNDIRECTED_NODES_CSV_FILE, UNDIRECTED_EDGES_CSV_FILE)
