'''
Module with a few utility functions to generate pdfs and print them using CLI
'''

import os
import subprocess

from config import DEFAULT_PRINTER

abs_dir = os.getcwd()
DEFAULT_PDF_FILE = "tmp/index.pdf"
DEFAULT_HTML_FILE = "tmp/index.html"

def make_html(s):
    '''Makes a HTML file out of a string argument
    '''
    with open(DEFAULT_HTML_FILE, 'w', encoding="utf-8") as html:
        html.write(s)
    html.close()

def exec_bash_command(cmd):
    '''Executes the bash command passed as argument
    '''
    process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    return output, error

def print_pdf_file(printer = DEFAULT_PRINTER, file = DEFAULT_PDF_FILE):
    '''Sends a PDF file to a printer for printing using lp
    '''
    cmd = "lp -d " + printer + " " + file
    print(cmd)
    exec_bash_command(cmd)

def gen_pdf_file(src = DEFAULT_HTML_FILE, dst = DEFAULT_PDF_FILE):
    '''Uses chromium bash command to output a pdf file from a html page
    '''
    exec_bash_command("chromium --headless --print-to-pdf=" + dst + " " + src)

def send_to_printer(s, verbose=True):
    '''Generates a PDF out of a string and then sends it to the printer
    '''
    make_html(s)
    if verbose :
        print("Generating PDF file")
    gen_pdf_file()

    if verbose :
        print("Printing PDF file")
    print_pdf_file()

def has_ongoing_jobs():
    '''Checks if a file is already being in the print for printing or not
    '''
    output, error = exec_bash_command("lpstat")
    if not error:
        return bool(output)
