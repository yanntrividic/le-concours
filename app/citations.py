'''
Module that iterates through all the people who were mentioned during the interviews
but that are not interviewees, and generates a PDF for each person. This file contains
all the details of the citations, and an anonymized graph that shows how the person
fits in the network. The titles within the PDF are anonymized versions of the
interviewees' names.
'''

import os.path
import subprocess
import unidecode

from pyppdf import save_pdf

from citations_server import PORT
from server import get_all_names_surnames, ODS_FILE, get_all_interviewees
from data_utils import get_interviewees_from_ods, get_data_from_ods

CITATIONS_PDF_FILE = 'tmp/citations.pdf'
PDF_FOLDER = 'tmp/pdf/'

args_dict = {"goto":{"waitUntil":'networkidle2', "timeout":1000000}}
#args_upd = {'emulateMedia':"screen", 'waitFor': '1000'}

def exec_bash_command(cmd):
    process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()

def gen_pdf_file(src, dst):
    exec_bash_command("chromium --dump-dom --virtual-time-budget=1000000 --disable-gpu --print-to-pdf="+
    os.path.dirname(os.path.abspath(__file__))+
    "/tmp/citations.pdf" + " " + src)

def get_names_not_interviewees():
    all_names = get_all_names_surnames(get_data_from_ods(ODS_FILE), join = False)
    print("all", all_names, "\n")
    all_names = set(tuple(name) for name in all_names)

    df_interviewees = get_interviewees_from_ods(ODS_FILE)
    interviewees_names = get_all_interviewees(df_interviewees, join = False)
    print("int", interviewees_names, "\n")
    interviewees_names = set(tuple(name) for name in interviewees_names)

    return set(all_names).difference(set(interviewees_names))

def pdf_already_generated(name):
    return os.path.exists(PDF_FOLDER+convert_name_to_file_name(name))

def convert_name_to_file_name(name):
    joined = '_'.join(name).replace(' ', '')
    unaccentuated = unidecode.unidecode(str(joined))
    lower_case = unaccentuated.lower()
    without_punct = lower_case.replace('-','').replace('’', '')
    return without_punct+".pdf"

def get_http_request(name):
    return "http://localhost:"+PORT+"?name="+name[0]+"&surname="+name[1]

if __name__ == '__main__':
    not_interviewees = get_names_not_interviewees()

    for name in not_interviewees:
        pdf_file = convert_name_to_file_name(name)
        if not pdf_already_generated(pdf_file):
            save_pdf(output_file=PDF_FOLDER+pdf_file, url=get_http_request(name), args_dict=args_dict)
            print(pdf_file + " correctly generated.")
        else:
            print(pdf_file + " already exists.")

    print("All the pdfs were generated.")
