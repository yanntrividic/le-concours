# app
\[FR\] Ce dossier contient une série de modules Python, ainsi que l'architecture logicielle construite autour d'eux, permettant de mener à bien la cartographie et l'étude des réseaux d'entraide à l'œuvre dans une communauté. Tous les programmes et toutes les dépendances utilisés pour générer les contenus de ce dépôt sont opensource et gratuites. Bien que ce dépôt fasse appel aux techologies du web, tout peut être utilisé sans avoir accès à Internet, et les données que vous avez soumises restent exclusivement en local.

\[EN\] This folder contains a series of Python modules alongside the software architecture build around them. Those programs allow the user to take on the mapping and the study of the help network that exists within a community. Each program and all the dependencies used to generate the content of this repo are free and opensource. Despite that this repo uses web technologies, everything can be used offline, and all the data you submitted will exclusively stay on your machine. Hit us up if you want this repo to be fully translated, we just haven't had the time (nor the use) to do it yet.

**Version :** 1.0  
**Auteur :** Yann Trividic  
**Licence :** Licence Art Libre

## Prérequis
Ces modules ont été développés en Python sur la version `20.04` d'Ubuntu. Vous aurez besoin d'une version `3.8.x` de Python pour les utiliser. Une série de dépendances est spécifiée dans le fichier `requirements.txt`. Afin de les installer, veuillez rentrer la commande `python3 -m pip install -r requirements.txt`. En plus de ces dépendances Python, vous aurez besoin de GeckoDriver afin de connecter FireFox a Selenium. Aussi, la dépendance JavaScript permettant d'automatiser la mise en page de certains documents, Paged.js, nécessite Chromium pour fonctionner au mieux. En plus d'installer ces dépendances, vous devrez éditer deux fichiers pour pouvoir utiliser les scripts sur vos données : `interviews.ods` et `config.py`.

### interviews.ods
Le fichier `interviews.ods` (en haut de l'arborescence du dépôt) constitue votre base de données. Ce tableur contient deux feuillets : le premier permet d'effectuer un suivi des interviews que vous avez menées, et le second permet de reporter une transcription des interviews pour que les données qu'elles renferment puissent être passées aux différents scripts. Ce document est à remplir en suivant les instructions du README principal du projet, ou en suivant votre propre méthode.

### config.py
Le module `config.py` permet de spécifier les valeurs de deux constantes. La première, `PATH_TO_GECKODRIVER`, vous permet d'indiquer où GeckoDriver est installé sur votre machine. La seconde, `DEFAULT_PRINTER` permet de spécifier le nom de votre imprimante afin d'automatiser l'impression des documents.

## Utiliser les scripts
Dans ce dépôt, vous pouvez exécuter principalement trois scripts : `server.py`, `interviews.py` et `citations.py`. Les autres modules contiennent soit des fonctions utilitaires, soit des sous-briques logicielles réutilisées dans les trois principaux exécutables.

### server.py
Ce module ne permet pas de générer de documents, il permet cependant d'explorer vos données sous une forme non anonymisée. Après avoir exécuté la commande `python3 server.py`, un serveur Bottle est mis en route localement sur votre machine sur le port `8080`. Celui-ci est accessible via l'URL `http://localhost:8080/`.

### interviews.py
À l'instar du script précédent, celui-ci fait aussi tourner un serveur Bottle, mais cette fois-ci sur le port `8081`. En vous rendant sur la page `localhost` associée, vous trouverez la transcription des interviews menées mises en page grâce à Paged.js. Les noms des personnes sont anonymisés en suivant les méthodes de cryptographie développées dans `titles.py`.

### citations.py
Le script `citations.py` fonctionne de concours avec le script `citations_server.py`. Ainsi, pour que `citations.py` puisse s'exécuter, il est nécessaire qu'une instance de `citations_server.py` soit en train de tourner. Ce dernier consiste en un serveur Bottle. Le rôle de `citations.py` est de générer un PDF pour chaque personne ayant été mentionnée lors des interviews et n'ayant pas elle-même été interviewée. Les détails des interviews sont recoupés, et un graphe correspondant à la manière dont s'intègre la personne dans le réseau est généré.

## Mentions additionnelles
En plus des dépendances précédemment citées, il est important de mentionner que la famille de polices utilisées pour ce projet est la [IBM Plex](https://www.ibm.com/plex/) conçue par Mike Abbink, et que les jolis drapeaux dans les mises en page ont été obtenus en partant du [code](https://github.com/nathanford/ragadjust) de Nathan Ford.