# -*- coding: utf-8 -*-

'''
Module that contains several Bottle driver utilities methods, and that
holds the architecture of a modest utilitary local website to navigate
through, clean, and organize the data.
'''

from bottle import route, request, run, static_file, redirect
import pandas as pd
import os
from data_utils import get_data_from_ods
from nltk.metrics.distance import edit_distance
from printer_utils import send_to_printer, has_ongoing_jobs
from g_gephi import write_cropped_graph_to_file, graph_to_soup
from bs4 import BeautifulSoup as bs

WITH_GRAPH = False # only works if a svg graph was generated using Gephi
ONLY_GRAPH = False # if True, will only display the graph
ONLY_PRINT = False # if True, details won't be displayed but only printed.
ONLY_EXTERNAL_MENTIONS = False # if True, will only display the coups de pouce where the person was 
# mentioned by somebody else

STYLES_PATHS = ['/static/css/style.css', '/static/fonts/ibm_plex/stylesheet.css']
DATA_FOLDER = 'data/'
CSV_FILE = 'data_cdp.csv'
ODS_FILE = '../interviews.ods'

BS4_SANITIZERS = [('&lt;', '<'), ('&gt;', '>'), ('&amp;', '&')]

####################################################################
# SERVER AND SERVER UTILS
####################################################################

@route('/')
@route('/login')
def login():
    return readlines_to_str("html/login.html")

@route('/login', method='POST')
@route('/login/<username>')
def do_login(username=""):
    post = request.forms.get('username')
    if post: username = post
    username = bytes(username, 'latin-1').decode('utf-8')
    print("debug", username)
    rows = get_rows_name_surname(df, username)
    print(rows)
    s = ""
    if rows.empty :
        most_probable = get_distance_matrix(username, get_all_names_surnames(df))
        if not most_probable.empty and most_probable['distance'].values[0] == 0:
            return (s + get_head_with_css(STYLES_PATHS) + "<body>\n\t<span><i>" + 
            username + " </i> n’a pas été cité par d'autres, mais a cité des personnes.</span>\n</body>")
        s = (s + get_head_with_css(STYLES_PATHS) + "<body>\n\t<span>Aucune personne du nom de <i>" + 
            username + 
            " </i> n’apparaît dans la base de données.\n</span>\n\t")
        if most_probable.empty :
            #si aucune correspondance, ou que le nom est cité mais que les rows sont vides
            return (s +
            make_button("Retour à l’accueil.", "no", "/confirm") + "\n</body>")
        else:
            print(most_probable['name'].values)
            most_probable = most_probable['name'].values[0]
            return (s + " Pourriez-vous être " + 
                make_button(most_probable, "yes", "/confirm") + "&#8239;? " +
                make_button("Non.", "no", "/confirm")+"</span>")
    else:
        #s = s +"<p class=\"vousetes\">Vous êtes " + username + "</p>"
        details = format_rows(rows)
        with open("html/details.html") as html:
            soup = bs(html, 'html.parser')
            elem = soup.find("span", {"id": "master_details"})
            for detail in details: elem.append(detail)
            insert_page_break(soup, soup.find("body"))
            if WITH_GRAPH:
                write_cropped_graph_to_file(username, 210, 297)
                body.append(graph_to_soup())
        html.close()
        s = sanitize(soup)
        if ONLY_GRAPH:
            redirect("/graph/" + username)
        if ONLY_PRINT:
            send_to_printer(s)
            redirect("/printing")
        else:
            return s + "<br>" + make_button("Retour à l'accueil.", "no", "/confirm")

@route('/printing')
def is_printing():
    '''Waits until the printing job is done, then redirects to /login
    '''
    if has_ongoing_jobs():
        return readlines_to_str("html/is_printing.html")
    else:
        redirect('/')

@route('/graph/<username>')
def get_graph(username=''):
    write_cropped_graph_to_file(username, 210, 297)
    return readlines_to_str('html/resized_graph.html')

@route('/confirm', method='POST')
def do_confirm():
    valyes = request.forms.get("yes")
    if valyes:
        redirect("/login/"+valyes)
    else:
        redirect("/")

@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root=os.path.dirname(__file__))

def readlines_to_str(filepath):
    file_str = ""
    with open(filepath, encoding="utf-8") as file:
        for line in file:
            file_str += line
    return file_str


####################################################################
# DATA UTILS
####################################################################

def get_head_with_css(filepaths):
    s = "<head>\n"
    for f in filepaths:
        s = s + "\t<link rel=\"stylesheet\" href=" + f + ">\n"
    return s +"</head>\n"

def get_low_df(df):
    low = df.copy()
    for col in ['nom_auteur', 'prenom_auteur', 'nom_cite', 'prenom_cite']:
        low[col] = low[col].str.lower()
    return low

def get_rows_name_surname(df, name_surname, only_external_mentions=ONLY_EXTERNAL_MENTIONS):
    name_surname = name_surname.split(" ")
    name = name_surname[0]
    surname = " ".join(name_surname[1:])
    return get_rows(df, name, surname, only_external_mentions)

def get_rows(df, name, surname, only_external_mentions):
    low = get_low_df(df)
    name = name.lower()
    surname = surname.lower()

    if not only_external_mentions:
        # there's a bug, if somebody with two first names enters his first names, then it won't work.
        # to fix it, combine the first names and surnames and compare it with what was inputed
        #print(str(name_surname) + "\n" + name + "\n" + surname + "\n")
        indexes = low[(low['nom_auteur'] == surname) & (low['prenom_auteur'] == name)].index.tolist()
        indexes = indexes + low[(low['nom_cite'] == surname) & (low['prenom_cite'] == name)].index.tolist()
    else:
        indexes = low[(low['nom_cite'] == surname) & (low['prenom_cite'] == name)].index.tolist()

    print(name, surname)
    print(df)
    print(df.iloc[indexes]["details"])
    return df.iloc[indexes]["details"]

def format_rows(rows, with_br=True):
    soup = bs(features="lxml") #default features to get rid of the warning
    l = []
    rows = rows.dropna().tolist() #add .unique() to remove duplicates

    for idx in range(len(rows)):
        s = soup.new_tag('p', attrs={"class": "details", "contenteditable": "true"})
        s.string = str(rows[idx])
        if idx != len(rows) - 1: 
            s.string = s.string + "&#x202F;;"
        else:
            if s.string[-1] != ".": s.string = s.string + "."
        l.append(s)
        if with_br: l.append(soup.new_tag('br'))

    # rows_str = ""
    # 	rows_str = rows_str + "<span class=\"details\">" + row + "</p><br>"
    # return rows_str
    return l

def sanitize(soup):
    out = str(soup)
    meta_char = list(zip(*BS4_SANITIZERS))[0]
    meta_char_replacement = list(zip(*BS4_SANITIZERS))[1]
    # Fill this up with whatever additional meta characters you need to escape
    for i in range(len(meta_char)):
        out = out.replace(meta_char[i], meta_char_replacement[i])
    return out

def insert_page_break(soup, page_break_tag):
    for _ in range(2):
        page_break_tag = soup.new_tag('p', attrs={"style": "page-break-after: always;display: block"})
        page_break_tag.string=str(" ")
        soup.append(page_break_tag)

def make_button(s, name, route):
    '''make this string as a button'''
    return ("<form action=\"" + route + "\" method=\"POST\" ><button type=\"submit\" name=\"" 
        + name + "\" value=\"" + s + "\">" + s + "</button></form>")

def get_all_names_surnames(df, only_interviewed = False, join=True):
    '''Extracts all names from the dataframe and formats it
    '''
    colnames = ["name", "surname"]
    df1 = df[['prenom_auteur', 'nom_auteur']]
    df1.columns = colnames

    if not only_interviewed:
        df2 = df[['prenom_cite', 'nom_cite']]
        df2.columns = colnames
        df_names_surnames = pd.concat([df1, df2], ignore_index=True, axis=0)
    else:
        df_names_surnames = df1

    l = df_names_surnames.dropna().drop_duplicates().values.tolist()

    if join:
        joined = []
        for name_surname in l:
            joined.append(" ".join(name_surname))
        return joined
    else: return l

def get_all_interviewees(df, join=True):
    # Initialisation of the DataFrame
    colnames = ["id", "name", "surname"]
    df_interviewees = df[['id', 'prenom', 'nom']]
    df_interviewees.columns = colnames

    # the na values and duplicated are removed
    df_interviewees = df_interviewees.dropna().drop_duplicates()

    # sort by id
    df_interviewees = df_interviewees.sort_values(by=['id'])

    # drop the id column
    df_interviewees = df_interviewees.drop(['id'], axis=1)

    #print(df_interviewees)

    l = df_interviewees.values.tolist()

    if join:
        joined = []
        for name_surname in l:
            joined.append(" ".join(name_surname))

        return joined
    else:
        return l

def get_distance_matrix(name, names):
    distance_matrix = pd.DataFrame(columns=['name', 'distance'])
    l = []
    for n in names:
        newrow = {'name': n, 'distance': edit_distance(name.lower(), n.lower())}
        l.append(newrow)
    distance_matrix = pd.concat([distance_matrix, pd.DataFrame.from_records(l)])
    print(distance_matrix.sort_values('distance'))
    return distance_matrix.sort_values('distance')[distance_matrix["distance"] <= 5]


####################################################################
# MAIN
####################################################################

if __name__ == '__main__':
    df = get_data_from_ods(ODS_FILE)
    #print(df.head(100))
    run(host='localhost', port=8080, debug=True)
    