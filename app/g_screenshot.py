# coding=utf-8

'''
Module that takes a unique function that screenshots a HTML pyvis graph using GeckoDriver
and saves it as a PNG file.
'''

import time
import os

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service

from g_pyvis import HEIGHT_GRAPH, WIDTH_GRAPH
from config import PATH_TO_GECKODRIVER

HEADLESS = True
MAX_COUNT = 1

def get_screenshot_graph(url, path):
    '''Opens a headless Geckodriver instance, opens a pyvis graph as an HTML file, lets it load
    correctly and takes a screenshot as a PNG image.
    '''
    options = Options()
    options.headless = HEADLESS
    s = Service(PATH_TO_GECKODRIVER, log_path=os.path.dirname(os.path.abspath(__file__))+"/tmp/geckodriver.log")

    driver = webdriver.Firefox(
        service=s,
        options=options)

    driver.set_window_position(0, 0)
    driver.set_window_size(int(WIDTH_GRAPH), int(HEIGHT_GRAPH)+100)

    driver.get(url) # has to be a complete URL, like "file:///..."

    display = "block"
    count = 0
    while display != "none" and count<MAX_COUNT:
        lb = driver.find_element(By.ID, "loadingBar")
        text = driver.find_element(By.ID, "text").text
        if text == "0%":
            count = count + 1
        display = lb.value_of_css_property("display")
        time.sleep(1)
        print(display)

    driver.execute_script("document.getElementById('text').style.opacity = 0;")
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    driver.save_screenshot(path)
    driver.quit()

if __name__ == "__main__":
    # test drive
    get_screenshot_graph("file://"+os.path.dirname(os.path.abspath(__file__))+"/tmp/nx_graph.html", "tmp/screenshot.png")
