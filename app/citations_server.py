'''
Implements a Bottle server to use in conjunction with the
citations.py module in order to generate a series of PDF.
See citations.py.
'''

import os

from bottle import get, route, run, request
from bs4 import BeautifulSoup as bs

from g_pyvis import gen_graph_html, TMP_GRAPH_FILE, write_to_file
from g_screenshot import get_screenshot_graph
from server import (ODS_FILE, readlines_to_str, sanitize)
from data_utils import get_data_from_ods
from titles import get_title, TITLE_MODE
from interviews import insert_details_html

df = get_data_from_ods(ODS_FILE)

CITATIONS_HTML_TEMPLATE = 'html/citations.html'
CITATIONS_TMP_FILE = 'tmp/citations.html'
GRAPH_IMG_PATH = 'tmp/screenshot.png'

PORT = "8082"

@get('/')
def graph():
    first_name = bytes(request.query['name'], 'latin-1').decode('utf-8')
    last_name = bytes(request.query['surname'], 'latin-1').decode('utf-8')
    name = [first_name, last_name]
    print(name)

    gen_graph_html(df, name)
    file_path = "file://"+os.path.dirname(os.path.abspath(__file__))+"/"+TMP_GRAPH_FILE
    get_screenshot_graph(file_path, GRAPH_IMG_PATH)
    make_interview(df, name)
    return readlines_to_str(CITATIONS_TMP_FILE)

def make_interview(df, name, show_title=True):
    print("enter_interview")
    with open(CITATIONS_HTML_TEMPLATE, encoding='utf-8') as html:
        soup = bs(html, 'html.parser')
        interview = soup.find("section", {"id": "interview"})
        if show_title:
            title = soup.new_tag("h1", attrs={"class": "name"})
            if TITLE_MODE == "names": 
                title.string = " ".join(name)
            else:
                title.string = get_title(name=name)
            interview.append(title)

        insert_details_html(df, name, interview)
        #insert_page_break(soup, interviews)

        graph_tag = soup.find("section", {"class": "graph"})
        img = soup.new_tag("img", attrs = {"src": "/static/"+GRAPH_IMG_PATH})
        graph_tag.append(img)

    html.close()
    soup = soup.prettify()
    s = sanitize(soup)
    print(s)
    write_to_file(s, CITATIONS_TMP_FILE)

@route('/raw')
def raw_graph():
    return readlines_to_str('tmp/nx_graph.html')

if __name__ == '__main__':
    run(host='localhost', port=PORT, debug=True)    
