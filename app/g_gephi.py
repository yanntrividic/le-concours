'''
Module that extracts a part of a Gephi SVG export.
This part is made out of a viewbox, and can be written to
another file.
'''

from bs4 import BeautifulSoup as bs
import os

graph_file = 'img/graph.svg' #relative path for the svg file
tmp_graph_file = 'tmp/tmp_graph.svg'
tmp_graph_absolute_path = os.path.dirname(os.path.realpath(__file__)) + '/' + tmp_graph_file

def get_element_from_name(name):
	with open(graph_file) as html:
		soup = bs(html, features='xml')
		elem = soup.find_all(lambda tag: tag.name == 'text' and name in tag.text)[0]
	html.close()
	return elem

def get_x_y_coordinates(name):
	elem = get_element_from_name(name)
	return float(elem['x']), float(elem['y'])

def get_svg_element():
	with open(graph_file) as html:
		soup = bs(html, features='xml')
		svg = soup.find('svg')
	html.close()
	return svg

def get_svg_viewbox():
	viewBox = get_svg_element()['viewBox']
	viewBox = viewBox.split(' ')
	c = dict()
	c['left'] = float(viewBox[0])
	c['top'] = float(viewBox[1])
	c['width'] = float(viewBox[2])
	c['height'] = float(viewBox[3])
	return c

def compute_viewbox_coor(name, w, h):
	x, y = get_x_y_coordinates(name)
	viewBox = get_svg_viewbox()
	newCoor = {'top': y - h/2, 'left': x - w/2, 'height': h, 'width': w}
	return newCoor

def get_cropped_graph(name, w, h, square=False):
	dict_coor = compute_viewbox_coor(name, w, h)

	# get a string with the new coordinates
	str_coor = ''
	for k in ['left', 'top', 'width', 'height']:
		str_coor = str_coor + str(dict_coor[k]) + " "

	if square:
		dim = 0
		if dict_coor['height'] > dict_coor['width']:
			dim = dict_coor['height']
		else:
			dim = dict_coor['height']

	with open(graph_file) as html:
		soup = bs(html, features='xml')
		svg = soup.find('svg')
		svg['viewBox'] = str_coor
		svg['height'] = dim if square else h
		svg['width'] = dim if square else w
	html.close()

	return str(soup)

def write_to_file(soup, file):
	#open text file
	text_file = open(file, "w")
	#write string to file
	text_file.write(soup)
	#close file
	text_file.close()

def write_cropped_graph_to_file(name, w, h):
	s = get_cropped_graph(name, w, h)
	write_to_file(s, tmp_graph_file)

def graph_to_soup():
	soup = bs()

	overlay = soup.new_tag('div', attrs={"class": "overlay"})
	soup.append(overlay)

	svg = soup.new_tag('div', attrs={"class": "svg"})
	svg.append(soup.new_tag('img', attrs={"src": tmp_graph_absolute_path}))
	soup.append(svg)

	return soup

if __name__ == '__main__':
	print(get_x_y_coordinates("Jeanne Petit"))
	print(get_svg_viewbox())
	print(compute_viewbox_coor("Jeanne Petit", 100, 200))
