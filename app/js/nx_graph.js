

    // initialize global variables.
    var edges;
    var nodes;
    var network; 
    var container;
    var options, data;

    
    // This method is responsible for drawing the graph, returns the drawn network
    function drawGraph() {
        var container = document.getElementById('mynetwork');

        // parsing and collecting nodes and edges from the python
        nodes = new vis.DataSet([{"color": "#000000", "id": "Antoine Farnier"}, {"color": "#000000", "id": "Camille Loreac"}, {"color": "#000000", "id": "Charlotte Lilino"},]);
        edges = new vis.DataSet([{"from": "Camille Loreac", "to": "Charlotte Lilino"}, {"from": "Camille Loreac", "to": "Antoine Farnier"}]);

        // adding nodes and edges to the graph
        data = {nodes: nodes, edges: edges};

        var options = {
    "configure": {
        "enabled": false
    },
    "edges": {
        "color": {
            "inherit": true
        },
        "smooth": {
            "enabled": true,
            "type": "continuous"
        }
    },
    "interaction": {
        "dragNodes": true,
        "hideEdgesOnDrag": false,
        "hideNodesOnDrag": false
    },
    "physics": {
        "enabled": true,
        "forceAtlas2Based": {
            "avoidOverlap": 0,
            "centralGravity": 0.01,
            "damping": 0.4,
            "gravitationalConstant": -150,
            "springConstant": 0.08,
            "springLength": 100
        },
        "solver": "forceAtlas2Based",
        "stabilization": {
            "enabled": true,
            "fit": true,
            "iterations": 1000,
            "onlyDynamicEdges": false,
            "updateInterval": 50
        }
    }
};
        
        

        

        network = new vis.Network(container, data, options);
	 
        


        
        network.on("stabilizationProgress", function(params) {
      		document.getElementById('loadingBar').removeAttribute("style");
	        var maxWidth = 496;
	        var minWidth = 20;
	        var widthFactor = params.iterations/params.total;
	        var width = Math.max(minWidth,maxWidth * widthFactor);

	        document.getElementById('bar').style.width = width + 'px';
	        document.getElementById('text').innerHTML = Math.round(widthFactor*100) + '%';
	    });
	    network.once("stabilizationIterationsDone", function() {
	        document.getElementById('text').innerHTML = '100%';
	        document.getElementById('bar').style.width = '496px';
	        document.getElementById('loadingBar').style.opacity = 0;
	        // really clean the dom element
	        setTimeout(function () {document.getElementById('loadingBar').style.display = 'none';}, 500);
	    });
        

        return network;

    }

    drawGraph();

