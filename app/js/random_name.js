function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

window.onload = function generatePlaceholder(){
	var name1 = ["Armand", "Adan", "Rolland", "André", "Forest", "Chen", "Khaled", "Gordon", "Bart", "Rachelle", "Jackie", "Aline", "Rebecca", "Sylvia", "Loretta", "Barbara", "Louisa", "Judith"];
	var name2 = ["Dupont", "Bibelot", "Vacances", "Chamoux", "Balla", "Birtoux", "Drucy", "Mircello", "Bottega", "Frussi", "dos Portas", "Diabi", "Kaba", "Zhong"];

	var name = name1[getRandomInt(0, name1.length)] + ' ' + name2[getRandomInt(0, name2.length)];

	document.getElementById("quietesvous").placeholder = name; 
	console.log(name);
}