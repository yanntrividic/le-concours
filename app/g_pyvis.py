'''
Module that contains a series of functions to help generate
a pyvis graph out of the data as a HTML file.
'''

from pyvis.network import Network
import pandas as pd
from bs4 import BeautifulSoup as bs

from data_utils import get_data_from_ods
from server import (get_low_df, ODS_FILE)

COLOR = '#000000'
SHAPE = 'dot'
TMP_GRAPH_FILE = 'tmp/nx_graph.html'
TMP_GRAPH_DATA = 'tmp/nx_graph.js'
GRAPH_TEMPLATE_FILE = 'html/nx_graph.html'
DEPTH = 1

HEIGHT_GRAPH = '900'#'197mm' #'197mm' # height available is 197mm
WIDTH_GRAPH = '900'#'93mm'
# the content page area less 1 mm

EDGE_THICKNESS = 4

def get_neighbors(df, name, surname, join=False):
    low = get_low_df(df)
    name = name.lower()
    surname = surname.lower()

    colnames = ['name', 'surname']

    indexes_author = low[(low['nom_auteur'] == surname) & (low['prenom_auteur'] == name)].index.tolist()
    df_cdp = df.iloc[indexes_author][['prenom_cite', 'nom_cite']]
    df_cdp.columns = colnames

    indexes_cdp = low[(low['nom_cite'] == surname) & (low['prenom_cite'] == name)].index.tolist()
    df_authors = df.iloc[indexes_cdp][['prenom_auteur', 'nom_auteur']]
    df_authors.columns = colnames

    df_neighbors = pd.concat([df_cdp, df_authors], ignore_index=True, axis=0)

    l = df_neighbors.dropna().drop_duplicates().values.tolist()

    if join:
        joined = []
        for name_surname in l:
            joined.append(" ".join(name_surname))

        return joined

    else: return l

def dfs(visited, df, node, depth):  #function for dfs
    nodes = set()
    edges = set()
    if ' '.join(node) not in visited and depth >= 0:
        visited.add(' '.join(node))
        nodes.add(' '.join(node))
        for neighbor in get_neighbors(df, node[0], node[1]):
            nodes.add(' '.join(neighbor))
            if neighbor[1] < node[1]:
                edges.add((' '.join(neighbor), ' '.join(node)))
            else:
                edges.add((' '.join(node), ' '.join(neighbor)))
            n_nodes, n_edges = dfs(visited, df, neighbor, depth-1)
            nodes = nodes.union(n_nodes)
            edges = edges.union(n_edges)
    return nodes, edges

def get_graph(df, name):
    nodes, edges = dfs(set(), df, name, DEPTH)
    nodes = list(nodes)

    print("getgraph", nodes, edges)

    nt = Network(HEIGHT_GRAPH+'px', WIDTH_GRAPH+'px')
    nt.add_nodes(nodes = nodes,
        color = [COLOR for _ in range(len(nodes))],
        value = [50 for _ in range(len(nodes))],
        shape = [SHAPE for _ in range(len(nodes))])

    for edge in edges:
        nt.add_edge(edge[0], edge[1], width = EDGE_THICKNESS)

    for idx in nt.get_nodes():
        node = nt.get_node(idx)
        node['label'] = False

    nt.force_atlas_2based(gravity=-150)
    #nt.toggle_stabilization(True)
    #nt.toggle_physics(False)
    nt.set_edge_smooth('continuous')
    #nt.show(TMP_GRAPH_FILE)
    nt.write_html(TMP_GRAPH_FILE)

def get_graph_style_tag():
    with open(TMP_GRAPH_FILE) as html:
        soup = bs(html, features='xml')
        elem = soup.find_all("style")[0]
    html.close()
    return elem
    # reinject in html; and then edit the style, in JS?

def gen_graph_script_tag():
    with open(TMP_GRAPH_FILE) as html:
        soup = bs(html, features='xml')
        elem = soup.find_all("script")[1]
    html.close()
    write_to_file(elem.string, TMP_GRAPH_DATA)

def get_graph_script_tag():
    with open(TMP_GRAPH_FILE) as html:
        soup = bs(html, features='xml')
        elem = soup.find_all("script")[1]
    html.close()
    return elem

def write_to_file(str, file):
    #open text file
    text_file = open(file, "w")
    #write string to file
    text_file.write(str)
    #close file
    text_file.close()

def gen_graph_html(df, name):
    get_graph(df, name)
    with open(GRAPH_TEMPLATE_FILE) as html:
        soup = bs(html, features='xml')

        style = get_graph_style_tag()
        head = soup.find('head')
        head.append(style)

        script = get_graph_script_tag()
        body = soup.find('body')
        body.append(script)

    html.close()
    write_to_file(str(soup.prettify()), TMP_GRAPH_FILE)
    return str(soup.prettify())

if __name__ == "__main__":
    df = get_data_from_ods(ODS_FILE)
    get_graph(df, ["Jeanne", "Petit"])
    print(gen_graph_html(df, ["Jeanne", "Petit"]))
    