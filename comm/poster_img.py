'''
Generates the image that was used for the poster
'''

from pyvis.network import Network
import networkx as nx

POSTER_IMG_FILE = "poster_img.html"

NODES = ["yann", "romain", "domitille", "clement", "emma", 
        "yannis", "sequoia", "camille", "victoire", "violette", 
        "marie", "leonore", "daniel", "claire", "lea", "pa", "charline"]

EDGES = [("yann", "romain"),
        ("romain", "domitille"),
        ("domitille", "emma"),
        ("domitille", "clement"),
        ("emma", "yannis"),
        ("clement", "charline"),
        ("clement", "pa"),
        ("pa", "charline"),
        ("charline", "yannis"),
        ("pa", "yannis"),
        ("yannis", "camille"),
        ("yannis", "sequoia"),
        ("sequoia", "victoire"),
        ("victoire", "camille"),
        ("victoire", "violette"),
        ("violette", "marie"),
        ("marie", "leonore"),
        ("leonore", "daniel"),
        ("daniel", "marie"),
        ("marie", "claire"),
        ("daniel", "claire"),
        ("leonore", "claire"),
        ("claire", "lea"),
        ("lea", "yann")]

EDGE_THICKNESS = 2

if __name__ == "__main__":
    G=nx.Graph()
    nt = Network('1080px', '1920px')

    nt.add_nodes(nodes=NODES,
        color=['#000000' for _ in range(len(NODES))],
        value = [100 for _ in range(len(NODES))],
        shape = ["dot" for _ in range(len(NODES))])

    for edge in EDGES:
        nt.add_edge(edge[0], edge[1], width = EDGE_THICKNESS)

    nt.set_edge_smooth('continuous')

    for idx in nt.get_nodes():
        node = nt.get_node(idx)
        node['label'] = True

    nt.force_atlas_2based(gravity=-150)
    nt.set_edge_smooth('continuous')
    nt.show(POSTER_IMG_FILE)
    