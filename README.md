<h1 id="title">Le concours</h1>
      <section id="definition">
         <p><i><b>concours</b></i> \kɔ̃ .kuʁ\ nom masculin</p>
         <ol>
            <li><u>Action</u> de <u>tendre</u> vers un <u>même</u> but, de <u>coopérer</u>.</li>
         </ol>
      </section>
      <section id="column">
         <section id="intro">
            <p>[FR] Le présent dépôt vise à partager un outil ayant été
               imaginé dans le but de mettre en exergue et de cartographier les
               relations d’entraide entre les individu·e·s
               d’une même communauté. Ce projet a été éprouvé dans le contexte d’une école d’art
               française en interviewant les étudiant·e·s
               s’apprêtant à passer leur diplôme de fin d’études (et dont voici 
               <a href="https://vimeo.com/manage/videos/730379788">la restitution</a>), mais il
               est certain qu’il pourrait être transposé dans une myriade
               d’autres environnements (entreprises, établissements publics,
               etc.). N’hésitez pas à prendre contact avec nous si
               vous souhaitez bénéficier de conseils pour adapter <i>Le
               concours</i> à vos propres besoins. Pour le README technique s'attelant à l'explication des différents scripts
            archivés dans ce dépôt, veuillez vous rendre dans le dossier <code>app</code>.
            </p>
               <p>[EN] The purpose of this repository is to share a tool that was designed with the idea of showing and mapping the relationships between individuals that belong to a same community. Even though this project was tested in the context of a french art school by interviewing the students about to finish their studies (here is a video of <a href="https://vimeo.com/manage/videos/730379788">the restitution</a>), we are positive that it could be transposed to a plethora of other environments (companies, public establishments, etc.). Don't hesitate getting in touch with us if you wish to get help to adapt it to your needs. Also, hit us up if you want this repo to be translated, we just haven't had the time (nor the use) to do it yet. Concerning a more technical README, please see the <code>app</code> folder.
            </p>
         </section>
         <h1>Prérequis</h1>
         <h2>Intervieweur·euses</h2>
         <p>Cet outil s’adresse <i>a priori</i> à toute personne souhaitant
            mieux comprendre les relations interpersonnelles régissant une
            communauté dont elle-même fait partie. Une personne, ou un groupe
            de personnes, doit endosser le rôle d’intervieweur·euse
            et être en capacité de passer du temps à discuter avec un nombre
            important d’interviewé·es. Le dispositif est effectif si les intervieweur·
            euses
            et les interviewé·es
            appartiennent à un même groupe d’individu·es
            au sein de la communauté, sans qu’il n’y ait aucun rapport
            hiérarchique entre elles·eux.
         </p>
         <p>Tenez un journal de bord. Si votre projet implique le concours de
            plusieurs intervieweur·euses,
            alors il sera d’autant plus nécessaire que ces personnes puissent
            travailler sur des documents communs et de la manière la plus
            transparente possible. Dans tous les cas, tenez un journal de bord.
         </p>
         <h2>Interviewé·es</h2>
         <p>L’étape la plus décisive de votre projet consiste en
            l’élaboration de la liste de personnes à interviewer. Une liste
            déséquilibrée fera transparaître des biais indésirables dans vos
            résultats&#x202F;; pour prévenir un tel écueil, il s’agira
            d’établir un ou plusieurs critères permettant de&nbsp;:
         </p>
         <ol>
            <li>obtenir un échantillon aussi représentatif que possible&#x202F;;</li>
            <li>constituer une liste de personnes d’une taille
               commensurable.
            </li>
         </ol>
         <p>Sans formuler ces critères pseudo-objectifs, vous prenez le
            risque d’oublier les individu·es
            les plus en marge de votre communauté, par exemple les personnes de
            nature discrète, ou qui sont peu présentes sur place. Dans le cadre
            d’une école, un critère possible pourrait être comme suit&nbsp;:
            «&#x202F;les interviewé·es
            sont étudiant·es
            en 3<sup>e</sup> année&#x202F;».
         </p>
         <p>Si vous-même vous répondez à tous les critères énoncés,
            incluez-vous dans la liste&nbsp;: vous prendrez alors ponctuellement
            la place de l’interviewé·e.
            Dans le cas où vous seriez la·le
            seul·e
            intervieweur·euse,
            prenez le temps de former quelqu’un·e
            à la méthode pour l’occasion&#x202F;; idéalement, la personne qui
            vous interrogera aura elle-même été interviewée au préalable.
         </p>
         <h2>Recollement</h2>
         <p>Après avoir décidé des critères de sélection, un travail de
            recollement reste à effectuer si vous n’avez pas un accès direct
            à une liste exhaustive des patronymes des personnes répondant aux
            conditions énoncées. Voici une série d’approches envisageables
            et non exclusives&nbsp;:
         </p>
         <ul>
            <li>explorez les serveurs informatiques susceptibles selon vous
               de receler des données pertinentes (un outil précieux peut
               notamment être le serveur associé à votre adresse mail
               institutionnelle)&#x202F;;
            </li>
            <li>tirez profit des réseaux sociaux&#x202F;;</li>
            <li>faites fonctionner l’effet de réseau en demandant à votre
               cercle de connaissances qui sont les personnes correspondant aux
               critères (aussi, n’hésitez pas à parler et à faire parler de
               votre projet, cela vous facilitera la tâche par la suite)&#x202F;;
            </li>
            <li>demandez cette liste à différentes personnes susceptibles
               d’y avoir accès, par exemple à des employé·es
               de votre administration.
            </li>
         </ul>
         <p>Concernant cette dernière option, nous préférons prévenir que
            le personnel de l’administration, sauf erreur de sa part, ne vous
            communiquera pas ces listes. En effet, ces données ne sont pas
            publiques en vertu du règlement général sur la protection des
            données (RGPD). Cela sera certainement à vous de croiser les sources,
            de tirer des déductions et d’imaginer vos propres protocoles pour
            assembler cette liste. Le moyen le plus efficace pour arriver à vos
            fins est très probablement de faire jouer l’effet de réseau. En
            plus de contourner la problématique du RGPD, cette approche permet
            de grandement faciliter l’étape suivante&nbsp;: la prise de
            contact avec les interviewé·es.
         </p>
         <h1>Interviews</h1>
         <h2>Prendre contact</h2>
         <p>Dès que votre liste de personnes s’est un peu étoffée, vous
            pouvez commencer les interviews. Pour ce faire, privilégiez au
            maximum une première interaction en personne&#x202F;; une prise de
            contact incarnée aura toujours plus de valeur qu’un simple mail.
            Afin de faciliter cette rencontre, l’effet de réseau est presque
            la seule option&nbsp;: en rassemblant les coordonnées d’un·e
            interviewé·e
            par l’intermédiaire de son entourage, vous pouvez savoir où et
            quand la·le
            trouver, et à quoi elle·il
            ressemble.
         </p>
         <p>La première prise de contact est très importante pour la suite
            du processus&nbsp;: elle mettra en confiance votre interviewé·e,
            ou non. Si vous ne connaissez pas au préalable la personne, un bon
            moyen de désamorcer la situation d’emblée est de lui expliquer
            clairement comment vous l’avez trouvée et les raisons qui vous
            amènent. Encore une fois, l’effet de réseau peut aider en citant
            la personne qui vous a conduit à l’interviewé·e,
            cela ayant pour conséquence de révéler un chemin clair vous
            reliant dans la communauté.
         </p>
         <p>La plupart du temps, le premier contact avec l’interviewé·e
            ne sera pas le moment le plus favorable pour l’interview. À moins
            qu’elle·il
            manifeste en connaissance de cause que le moment est adéquat pour
            elle·lui,
            proposez une rencontre pour plus tard. Afin que l’organisation du rendez-vous se déroule au mieux
            et puisse s’adapter aux aléas des emplois du temps, essayez
            d’obtenir un moyen fiable de recontacter la personne, en favorisant
            le téléphone. Ce média s’est révélé être le plus efficace
            dans le cadre de l’école d’art.
         </p>
         <p>Soyez arrangeant·e
            et convenez d’un moment faiblement contraint par la durée.
            Rendez-vous disponible dans la mesure du possible. Ne pressez votre
            interlocuteur·ice
            à aucun moment. La durée de l’interview étant définie par
            l’interviewé·e,
            il faut prévoir que celle-ci puisse s’étendre sur plusieurs
            heures. Bien sûr, il existe à cet égard autant de règles que de
            situations. 
         </p>
         <h2>Discuter</h2>
         <p>Comme explicité dans la section précédente, la durée de
            l’interview est libre. Dans le cadre de l’école d’art, les
            durées effectives ont été comprises entre 15&nbsp;min et
            3&#x202F;h&#x202F;30&#x202F;min, certaines personnes ayant besoin de plus
            de temps que d’autres. Ce n’est pas le seul aspect de l’interview
            qui est libre&#x202F;; cette dernière s’apparente dans la plupart
            des cas à une simple discussion où l’un des enjeux est de créer
            un espace de parole sain avec un large panel de sujets possibles.
         </p>
         <p>Munissez-vous d’un tirage du document <code>memento.pdf</code>
            disponible dans le dossier <code>comm</code>. Donnez aussi un exemplaire à votre
            interviewé·e afin
            qu’elle·il puisse s’y
            référer à sa guise. Partager un même document instaure un cadre
            de confiance et inscrit la rencontre dans un périmètre clair. La
            première page du document explique succinctement la visée du projet
            et agit surtout comme un support sur lequel l’interviewé·e
            peut se reposer pour faire émerger des souvenirs. À partir de là,
            vous pouvez initier l’interview en énonçant distinctement les
            deux questions constituant la trame du projet&nbsp;:
         </p>
         <ul>
            <li>«&#x202F;Qui sont les personnes qui t’ont aidé·e
               depuis que tu es ici&#x202F;?&#x202F;»&#x202F;;
            </li>
            <li>«&#x202F;Qui sont les personnes que tu as aidées depuis que
               tu es ici&#x202F;?&#x202F;».
            </li>
         </ul>
         <p>Dans la même idée qu’il faut trouver un échantillon de taille
            convenable pour que le projet reste réalisable, il faut aussi
            circonscrire l’étendue des possibles quant aux personnes pouvant
            être citées (et ce pour quoi elles peuvent être citées). En
            d’autres termes, il faut qualifier ce que vous considérez
            correspondre à de l’aide&#x202F;; cette notion changeant avec le
            contexte du projet. Le curseur sera à ajuster en fonction de la
            densité du réseau que vous cherchez à cartographier. Plus cette
            dernière est élevée et plus il faudra restreindre les critères.
            Modifiez le memento à votre guise pour correspondre à vos attentes.
            Prenez le temps de vérifier que votre interviewé·e
            ait bien saisi les nuances de ce que vous lui demandez.
         </p>
         <p>Lors de la discussion, ne cherchez pas à obtenir une liste de
            noms exhaustive. Si votre interviewé·e
            s’en inquiète, rassurez-la·le&nbsp;:
            c’est sa subjectivité – la conjonction de ses souvenirs et de
            ses choix – qui rend sa liste pertinente. Ainsi, ne tentez pas
            d’épuiser la discussion, accueillez simplement ce que l’on vous
            donne. Si l’interview dérive, saluez ce moment et voyez ce qui en
            ressort, soyez à l’écoute. De même, une fois que l’interviewé·e
            décide de mettre fin à la discussion, laissez-lui la possibilité
            de conserver le memento afin qu’elle·il
            puisse continuer l’interview seul·e
            et vous le rendre après l’avoir complété plus tard. Pour
            prévenir toute perte d’information, prenez en photo la
            transcription de l’interview dans son état courant avant de
            quitter l’interviewé·e.
         </p>
         <h2>Écrire</h2>
         <p>Afin de mettre la personne en face de vous la plus à l’aise
            possible, reportez l’interview uniquement à l’écrit. Laissez votre interviewé·e écrire
            si elle·il le préfère.
            N’utilisez pas d’enregistreur ou de caméra, ceux-ci
            pouvant brider ce que l’interviewé·e
            serait prêt·e à vous
            confier. Pour améliorer votre vitesse de transcription et coller au
            mieux à ce qui est dit oralement, adoptez des habitudes
            sténographiques (voir <a href="https://www.github.com/yanntrividic/de-la-ligne-a-la-note"><i>De la ligne à la note</i></a>).
         </p>
         <p>Le memento a été imaginé pour permettre la prise de notes des
            interviews. À partir de la page 2, le document est constitué d’une
            grille permettant de recentrer l’interview sur la liste des noms à
            constituer lorsqu’il est nécessaire. La grille est faite de
            différentes colonnes, les plus essentielles étant les colonnes <i>nom</i>,
            <i>prénom</i> et <i>détails</i>. Libre à vous d’ajouter les
            colonnes vous semblant pertinentes. La colonne <i>détails</i> est
            prévue pour accueillir une explication de l’aide donnée ou reçue
            par la personne citée. Une fois l’interview terminée, effectuez
            une nouvelle transcription vers le fichier <code>interviews.ods</code>
            prévu à cet effet.
         </p>
         <h1>Restituer</h1>
         <p>Les deux principaux scripts du dépôt, <code>interviews.py</code>
            et <code>citations.py</code>, ont été pensés
            exclusivement pour faciliter la restitution du contenu des
            interviews. Le premier, <code>interviews.py</code>,
            met en page tous les détails des aides données et reçues par les
            interviewé·es,
            interview par interview. Le document généré peut directement être
            imprimé.
         </p>
         <p>Le second script concerne quant à lui les personnes qui ont été
            citées mais qui n’ont pas été interviewées&nbsp;: le programme
            recoupe toutes les entrées dans lesquelles elles ont été nommées et
            les compile pareillement dans une mise en page prête à
            l’impression. En dernière page, un graphe est modélisé
            permettant de visualiser le réseau d’entraide auquel la personne
            prend part.
         </p>
         <p>Toutes les données sont anonymisées. Cette action permet de
            gommer toute envie de transformer l’outil en un objet de contrôle
            ou de fichage. Ainsi, chacun·e
            est libre de s’identifier ou non au contenu d’un des documents.
            Quelle que soit la forme prise par la restitution, il est crucial que
            les interviewé·es
            soient au courant des conditions de celles-ci avant l’interview. Il
            est aussi important d’obtenir leur accord quand les conditions de
            publicisation changent au cours du projet, ou quand les personnes
            sont nommées.
         </p>
         <p>La restitution du projet peut prendre de nombreuses formes (un
            texte, une discussion, une visite guidée, etc.). Imprimez les
            interviews si cela vous semble être adapté, les différents
            programmes du dépôt permettent cela. Cependant, n’hésitez pas à
            développer et à publier vos propres outils. Quoi que vous décidiez,
            rendez publics vos résultats à l’échelle de votre communauté.
            Parlez-en, invitez les décideur·
            euses
            de votre communauté ainsi que les différent·es
            acteur·ices
            qui la composent, que vous les ayez interviewé·es
            ou non. Discutez des problèmes qui transparaissent dans les
            interviews, visibilisez-les, et pensez des solutions. Partagez ces
            savoirs. Faites-en bon usage.
         </p>
         <h1>Licence</h1>
         <p>Ce README, tout comme l’ensemble des documents originaux
            référencés sur le dépôt GitLab du projet <i>Le concours</i>, est
            distribué sous la <a href="https://artlibre.org/">Licence Art Libre</a>,
            il s’agit donc d’un logiciel libre. Chacun·e
            a la possibilité d’adapter cet outil à ses besoins, de
            l’améliorer, de le repenser.
         </p>
         <img src="https://gitlab.com/yanntrividic/le-concours/-/raw/main/comm/poster_img_thickness_2.png">
      </section>
